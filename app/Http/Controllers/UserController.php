<?php

namespace App\Http\Controllers;

use App\User;
use App\Company;
use Illuminate\Http\Request;
use App\UserCompany;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/users';

    /**
     * Get the post create / update redirect path.
     *
     * @return string
     */
    public function redirectPath()
    {
        if (method_exists($this, 'redirectTo')) {
            return $this->redirectTo();
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/users';
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    /**
     * Display a listing of the users.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        $trashed_users = User::onlyTrashed()->get();

        return view('user.index', [
            'users' => $users,
            'trashed_users' => $trashed_users
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validator($request->all())->validate();

        $data = $request->all();

        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        return redirect($this->redirectPath());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = $this->getUserBy($id);

        $related_companies_id = UserCompany::select('company_id')
            ->where('user_id', $user->id)
            ->get();

        $companies = Company::whereNotIn('id', $related_companies_id)->get();

        return view('user.show', [
            'user' => $user,
            'companies' => $companies
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->getUserBy($id);

        return view('user.edit', [
            'user' => $user
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = $this->getUserBy($id);

        $this->validator($request->all())->validate();

        $data = $request->all();

        $user->update([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        return redirect($this->redirectPath());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = $this->getUserBy($id);

        if ($user->id == Auth::id()) {
            abort(403);
        }

        $user->delete();

        return redirect($this->redirectPath());
    }

    /**
     * Attaching a new company.
     *
     * @param  int  $user_id
     * @param  int  $company_id
     * @return \Illuminate\Http\Response
     */
    public function attach($user_id, $company_id)
    {
        $user = $this->getUserBy($user_id);

        $company = $this->getCompanyBy($company_id);

        if (!UserCompany::where(['user_id' => $user->id, 'company_id' => $company->id])->exists()) {
            $user->companies()->attach($company->id);
        }

        return redirect(route('users.show', ['id' => $user->id]));
    }

    /**
     * Detaching relation.
     *
     * @param  int  $user_id
     * @param  int  $company_id
     * @return \Illuminate\Http\Response
     */
    public function detach($user_id, $company_id)
    {
        $user = $this->getUserBy($user_id);

        $company = $this->getCompanyBy($company_id);

        if (UserCompany::where(['user_id' => $user->id, 'company_id' => $company->id])->exists()) {
            $user->companies()->detach($company->id);
        }

        return redirect(route('users.show', ['id' => $user->id]));
    }

    /**
     * Getting a company by ID.
     *
     * @param  int  $id
     * @return \App\Company
     */
    protected function getCompanyBy($id)
    {
        return Company::findOrFail($id);
    }

    /**
     * Getting a user by ID.
     *
     * @param  int  $id
     * @return \App\Company
     */
    protected function getUserBy($id)
    {
        return User::findOrFail($id);
    }
}
