<?php

namespace App\Http\Controllers;

use App\Company;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\UserCompany;

class CompanyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/companies';

    /**
     * Get the post create / update redirect path.
     *
     * @return string
     */
    public function redirectPath()
    {
        if (method_exists($this, 'redirectTo')) {
            return $this->redirectTo();
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/companies';
    }

    /**
     * Display a listing of the companies.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::all();

        $trashed_companies = Company::onlyTrashed()->get();

        return view('company.index', [
            'companies' => $companies,
            'trashed_companies' => $trashed_companies
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $this->validator($data)->validate();

        $data['user_id'] = Auth::id();

        Company::create($data);

        return redirect($this->redirectPath());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = $this->getCompanyBy($id);

        $related_users_id = UserCompany::select('user_id')
            ->where('company_id', $company->id)
            ->get();

        $users = User::whereNotIn('id', $related_users_id)->get();

        return view('company.show', [
            'company' => $company,
            'users' => $users
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = $this->getCompanyBy($id);

        return view('company.edit', ['company' => $company]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = $this->getCompanyBy($id);

        $data = $request->all();

        $this->validator($data)->validate();

        $company->company = $data['company'];
        $company->user_id = Auth::id();

        $company->save();

        return redirect($this->redirectPath());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = $this->getCompanyBy($id);

        $company->delete();

        return redirect($this->redirectPath());
    }

    /**
     * Attaching a new user.
     *
     * @param  int  $user_id
     * @param  int  $company_id
     * @return \Illuminate\Http\Response
     */
    public function attach($company_id, $user_id)
    {
        $company = $this->getCompanyBy($company_id);

        $user = $this->getUserBy($user_id);

        if (!UserCompany::where(['user_id' => $user->id, 'company_id' => $company->id])->exists()) {
            $company->users()->attach($user->id);
        }

        return redirect(route('companies.show', ['id' => $company->id]));
    }

    /**
     * Detaching relation.
     *
     * @param  int  $user_id
     * @param  int  $company_id
     * @return \Illuminate\Http\Response
     */
    public function detach($user_id, $company_id)
    {
        $company = $this->getCompanyBy($company_id);

        $user = $this->getUserBy($user_id);

        if (UserCompany::where(['user_id' => $user->id, 'company_id' => $company->id])->exists()) {
            $company->users()->detach($user->id);
        }

        return redirect(route('companies.show', ['id' => $company->id]));
    }

    /**
     * Getting a user by ID.
     *
     * @param  int  $id
     * @return \App\Company
     */
    protected function getUserBy($id)
    {
        return User::findOrFail($id);
    }

    /**
     * Getting a company by ID.
     *
     * @param  int  $id
     * @return \App\Company
     */
    protected function getCompanyBy($id)
    {
        return Company::findOrFail($id);
    }

    /**
     * Get a validator for an incoming store / update request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'company' => 'required|max:255|unique:company',
        ]);
    }
}