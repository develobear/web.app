<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CreateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company')->unique();
            $table->unsignedInteger('user_id');
            $table->softDeletes();
            $table->timestamps();
        });

        DB::table('company')->insert([
            ['company' => uniqid(), 'user_id' => 1, 'created_at' => Carbon::now()],
            ['company' => uniqid(), 'user_id' => 1, 'created_at' => Carbon::now()],
            ['company' => uniqid(), 'user_id' => 1, 'created_at' => Carbon::now()],
            ['company' => uniqid(), 'user_id' => 2, 'created_at' => Carbon::now()],
            ['company' => uniqid(), 'user_id' => 2, 'created_at' => Carbon::now()],
            ['company' => uniqid(), 'user_id' => 2, 'created_at' => Carbon::now()],
            ['company' => uniqid(), 'user_id' => 3, 'created_at' => Carbon::now()],
            ['company' => uniqid(), 'user_id' => 3, 'created_at' => Carbon::now()],
            ['company' => uniqid(), 'user_id' => 3, 'created_at' => Carbon::now()],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company');
    }
}
