@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-body">
                            <table class="table">
                                <thead class="thead-light">
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Company</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">{{ $company->id }}</th>
                                    <td>{{ $company->company }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">

                        <div class="card">
                            <div class="card-header">{{ __('Available Users') }} <span class="badge badge-pill badge-success">Click to attach</span></div>

                            <div class="card-body">
                                @if ($users->count() > 0)
                                    @foreach ($users as $user)
                                        <form method="POST" action="{{ route('companies.attach', ['company_id' => $company->id, 'user_id' => $user->id]) }}" style="display: inline-block; margin-bottom: 5px;">
                                            @csrf
                                            @method('PUT')
                                            <button class="btn btn-outline-success">{{ $user->email }}</button>
                                        </form>
                                    @endforeach
                                @else
                                    <p>No available users</p>
                                @endif
                            </div>
                        </div>

                </div>
                <div class="col-lg-4">

                        <div class="card">
                            <div class="card-header">{{ __('Related Users') }} <span class="badge badge-pill badge-danger">Click to detach</span></div>

                            <div class="card-body">
                                @if ($company->users()->count() > 0)
                                    @foreach ($company->users as $user)
                                        <form method="POST" action="{{ route('companies.detach', ['company_id' => $company->id, 'user_id' => $user->id]) }}" style="display: inline-block; margin-bottom: 5px;">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-outline-danger">{{ $user->email }}</button>
                                        </form>
                                    @endforeach
                                @else
                                    <p>No related users</p>
                                @endif
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
