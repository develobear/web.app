@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">List of companies</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="col-lg-12">

                        <a class="btn btn-primary" href="{{ route('companies.create') }}">Create</a>

                    </div><hr>
                    @if ($companies->count() > 0)
                        @foreach ($companies as $company)
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-12"><p>This is company {{ $company->company }}.</p></div>
                                    <div class="col-lg-4">
                                        <a class="btn btn-primary" href="/companies/{{ $company->id }}/edit">Edit</a>
                                        <a class="btn btn-primary" href="/companies/{{ $company->id }}">Show</a>
                                    </div>
                                    <div class="offset-lg-6 col-lg-2">
                                        <form method="POST" action="/companies/{{ $company->id }}">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-primary">Delete</button>
                                        </form>
                                    </div>
                                </div>
                            </div><hr>
                        @endforeach
                    @else
                        <div class="alert alert-error" role="alert">
                            Create a new company.
                        </div>
                    @endif

                </div>
            </div>
            <br>
            @if($trashed_companies->count() > 0)
                <div class="card">
                    <div class="card-header">List of deleted companies</div>

                    <div class="card-body">
                        @if ($trashed_companies->count() > 0)
                            @foreach ($trashed_companies as $company)
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-12"><span>This is company {{ $company->company }}.</span></div>
                                    </div>
                                </div><hr>
                            @endforeach
                        @else
                            <div class="alert alert-error" role="alert">
                                Create a new company.
                            </div>
                        @endif

                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
@endsection
