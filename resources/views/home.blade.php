@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Start page</div>

                <div class="card-body">
                    <a class="btn btn-primary" href="{{ route('companies.index') }}">List of Companies</a>
                    <a class="btn btn-primary" href="{{ route('users.index') }}">List of Users</a>
                    <a class="btn btn-primary" href="/docs" target="_blank">Documentation</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
