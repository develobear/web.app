@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">List of users.</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="col-lg-12">

                        <a class="btn btn-primary" href="{{ route('users.create') }}">Create</a>

                    </div><hr>
                    @if ($users->count() > 0)
                        @foreach ($users as $user)
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-12"><p>This is {{ $user->name }}.</p></div>
                                    <div class="col-lg-4">
                                        <a class="btn btn-primary" href="/users/{{ $user->id }}/edit">Edit</a>
                                        <a class="btn btn-primary" href="/users/{{ $user->id }}">Show</a>
                                    </div>
                                    <div class="offset-lg-6 col-lg-2">
                                        <form method="POST" action="/users/{{ $user->id }}">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-primary">Delete</button>
                                        </form>
                                    </div>
                                </div>
                            </div><hr>
                        @endforeach
                    @else
                        <div class="alert alert-error" role="alert">
                            Create a new user.
                        </div>
                    @endif
                </div>
            </div>
            <br>
            @if ($trashed_users->count() > 0)
            <div class="card">
                <div class="card-header">List of deleted users.</div>

                <div class="card-body">
                    @foreach ($trashed_users as $user)
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-12"><span>This is {{ $user->name }}.</span></div>
                            </div>
                        </div><hr>
                    @endforeach
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection
