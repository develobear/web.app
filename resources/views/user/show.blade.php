@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-header">{{ __('User info') }}</div>
                        <div class="card-body">
                            <div class="row">
                                <table class="table">
                                    <thead class="thead-light">
                                    <tr>
                                        <th scope="col">ID</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Email</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row">{{ $user->id }}</th>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-header">{{ __('Available Companies') }} <span class="badge badge-pill badge-success">Click to attach</span></div>

                        <div class="card-body">
                            @if ($companies->count())
                                @foreach ($companies as $company)
                                    <form method="POST" action="{{ route('users.attach', ['user_id' => $user->id, 'company_id' => $company->id]) }}" style="display: inline-block; margin-bottom: 5px;">
                                        @csrf
                                        @method('PUT')
                                        <button class="btn btn-outline-success">{{ $company->company }}</button>
                                    </form>
                                @endforeach
                            @else
                                <p>No available companies</p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-header">{{ __('Related Companies') }} <span class="badge badge-pill badge-danger">Click to detach</span></div>

                        <div class="card-body">
                            @if ($user->companies->count())
                                @foreach ($user->companies as $company)
                                    <form method="POST" action="{{ route('users.detach', ['user_id' => $user->id, 'company_id' => $company->id]) }}" style="display: inline-block; margin-bottom: 5px;">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-outline-danger">{{ $company->company }}</button>
                                    </form>
                                @endforeach
                            @else
                                <p>No related companies</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
