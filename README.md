
A simple application based on the Laravel framework.

Clone project:

git clone https://develobear@bitbucket.org/develobear/web.app.git

Create vendor:

php composer.phar install

Apply migrations:

php artisan migrate

Profit!