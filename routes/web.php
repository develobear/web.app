<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::resource('users', 'UserController');
Route::put('/users/{user_id}/company/{company_id}', 'UserController@attach')->name('users.attach');
Route::delete('/users/{user_id}/company/{company_id}', 'UserController@detach')->name('users.detach');

Route::resource('companies', 'CompanyController');
Route::put('/companies/{company_id}/user/{user_id}', 'CompanyController@attach')->name('companies.attach');
Route::delete('/companies/{company_id}/user/{user_id}', 'CompanyController@detach')->name('companies.detach');


